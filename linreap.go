package linnreap

import (
	"gitlab.com/golinnstrument/linnstrument"
)

func New128(dev linnstrument.Device) Connection {
	return newConnection("linreap128", dev, &linn128{})
}
