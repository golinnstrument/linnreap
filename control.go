package linnreap

import (
	"gitlab.com/golinnstrument/linnstrument"
)

type ControlButton [2]uint8 // first: row, second: color

func (c ControlButton) Row() uint8 {
	return c[0]
}

func (c ControlButton) Color() linnstrument.Color {
	return linnstrument.Color(c[1])
}

var (
	PerSplitSettings ControlButton = [2]uint8{0, MAGENTA.Value()}
	Preset           ControlButton = [2]uint8{1, CYAN.Value()}
	Volume           ControlButton = [2]uint8{2, LIME.Value()}
	Transpose        ControlButton = [2]uint8{3, ORANGE.Value()}
	Switch1          ControlButton = [2]uint8{4, WHITE.Value()}
	Switch2          ControlButton = [2]uint8{5, PINK.Value()}
	Split            ControlButton = [2]uint8{6, BLUE.Value()}
)

var controlButtons = []ControlButton{
	PerSplitSettings,
	Preset,
	Volume,
	Transpose,
	Switch1,
	Switch2,
}

type Mode interface {
	String() string

	PadPressed(x, y, velocity uint8)
	PadReleased(x, y uint8)
	Enter()
	Leave()

	//SetTriggerer(Triggerer)

	TransposePressed() linnstrument.Color
	TransposeReleased() linnstrument.Color

	Switch1Pressed() linnstrument.Color
	Switch1Released() linnstrument.Color

	SplitPressed() linnstrument.Color
	SplitReleased() linnstrument.Color
}
