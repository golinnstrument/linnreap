package linnreap

import (
	"fmt"
	"sync"

	"gitlab.com/golinnstrument/linnstrument"
	"gitlab.com/goosc/reaper/playtime"
)

var _ SubMode = &clipSubmode{}

func newClipSubmode(tr Triggerer, pt *PlaytimeMode) *clipSubmode {
	m := &clipSubmode{}
	m.tr = tr
	m.playtimeMode = pt
	m.playtime = playtime.New(14, 7, tr)
	m.resetSelectedActiveAndRecording()
	return m
}

func (c *clipSubmode) String() string {
	return "clip submode"
}

type clipSubmode struct {
	tr       Triggerer
	playtime *playtime.Playtime
	sync.RWMutex
	selectedClip [2]int8 // row, col negative means: nothing selected
	clipsFilled  [14][8]bool

	activeTrackClips [14]int8
	recordingClips   [14]int8
	playtimeMode     *PlaytimeMode
	isActiveSubmode  bool
}

func (c *clipSubmode) resetActiveAndRecording() {
	fmt.Println("resetActiveAndRecording")
	c.Lock()
	for i := 0; i < 14; i++ {
		c.activeTrackClips[i] = -1
		c.recordingClips[i] = -1
	}
	c.Unlock()
}

func (c *clipSubmode) resetSelectedActiveAndRecording() {
	c.Lock()
	c.selectedClip[0] = -1
	c.Unlock()
	c.resetActiveAndRecording()
}

func (c *clipSubmode) fillOrDeleteSelectedClip() {
	x, y, has := c.getSelectedClip()
	if !has {
		return
	}
	c.RLock()
	filledClips := c.clipsFilled
	activeClips := c.activeTrackClips
	recordingClips := c.recordingClips
	c.RUnlock()
	if filledClips[y][x] {
		// we can't reliably delete clips that are in the process of being recorded
		// so don't do anything
		if recordingClips[y] == int8(x) {
			return
		}
		//		fmt.Printf("delete clip %v/%v\n", x, y)
		c.Lock()
		c.clipsFilled[y][x] = false
		if activeClips[y] == int8(x) {
			c.activeTrackClips[y] = -1
		}
		c.Unlock()

		c.playtime.DeleteClip()

		if activeClips[y] == int8(x) {
			c.playtime.StopTrack(y + 1)
			c.playtimeMode.unsetRunningTrack(y)
		}

		//				c.Set(x, y, linnstrument.Off)
		c.highLightSelectedClip()
		return
	}
	c.Lock()
	c.clipsFilled[y][x] = true
	//	c.selectedClip[0] = -1
	c.Unlock()
	//	fmt.Printf("fill clip %v/%v\n", x, y)
	c.Set(x, y, colorFilledSelectedClip)
	//	c.unSelectClip()
}

func (c *clipSubmode) triggerSelectedClip() {
	x, y, has := c.getSelectedClip()
	if !has {
		return
	}
	c.playtimeMode.setRunningTrack(y)

	c.RLock()
	filledClips := c.clipsFilled
	c.RUnlock()

	// filled -> trigger playting
	if filledClips[y][x] {

		// trigger the selected clip
		c.playtime.TriggerClip()

		c.Lock()

		// last playing clip of same track
		old := c.activeTrackClips[y]

		// last recording clip of same track
		recording := c.recordingClips[y]

		// the triggered clip is now the active clip
		c.activeTrackClips[y] = int8(x)

		// if any clip was recording, this has been stopped now
		c.recordingClips[y] = -1
		c.Unlock()

		// set the color of the last playing clip
		if old >= 0 {
			c.Set(uint8(old), y, colorFilledClip)
		}

		// set the color of the last recording clip
		if recording >= 0 {
			c.Set(uint8(recording), y, colorFilledClip)
		}

		// set own color
		c.Set(x, y, colorActiveTrackSelected)
		return
	}

	// not filled -> trigger recording

	// trigger the selected clip
	c.playtime.TriggerClip()

	c.Lock()

	// last playing clip of same track
	old := c.activeTrackClips[y]

	// last recording clip of same track
	recording := c.recordingClips[y]

	// the triggered clip is now the active clip
	c.activeTrackClips[y] = int8(x)

	// if any clip was recording, this has been stopped now
	c.recordingClips[y] = int8(x)

	c.clipsFilled[y][x] = true
	c.Unlock()

	// set the color of the last playing clip
	if old >= 0 {
		c.Set(uint8(old), y, colorFilledClip)
	}

	// set the color of the last recording clip
	if recording >= 0 {
		c.Set(uint8(recording), y, colorFilledClip)
	}

	// set own color
	c.Set(x, y, colorClipRecordingSelected)
}

func (c *clipSubmode) paintClips() {
	c.RLock()
	filledClips := c.clipsFilled
	recordingClips := c.recordingClips
	activeClips := c.activeTrackClips
	c.RUnlock()

	for x := uint8(0); x < groupStopRow; x++ {
		for y := uint8(0); y < sceneLaunchCol; y++ {
			//c.playtimeMode.unsetRunningTrack(y)
			var color linnstrument.Color
			switch {
			case recordingClips[y] == int8(x):
				color = colorClipRecording
				//c.playtimeMode.setRunningTrack(y)
			case activeClips[y] == int8(x):
				color = colorActiveTrackClip
				//c.playtimeMode.setRunningTrack(y)
			case filledClips[y][x]:
				color = colorFilledClip
			default:
				color = OFF
			}

			c.Set(x, y, color)
		}
	}

	c.highLightSelectedClip()
}

func (c *clipSubmode) getSelectedClip() (x, y uint8, has bool) {
	c.RLock()
	selected := c.selectedClip
	c.RUnlock()
	if selected[0] < 0 {
		return 0, 0, false
	}
	return uint8(selected[0]), uint8(selected[1]), true

}

func (c *clipSubmode) highLightSelectedClip() {
	x, y, has := c.getSelectedClip()
	if !has {
		return
	}

	//	fmt.Printf("highlighting clip %v/%v\n", x, y)
	var color linnstrument.Color
	c.RLock()
	switch {
	case c.recordingClips[y] == int8(x):
		//		fmt.Println("hightlighted is recording clip")
		color = colorClipRecordingSelected
	case c.activeTrackClips[y] == int8(x):
		//		fmt.Println("hightlighted is active clip")
		color = colorActiveTrackSelected
	case c.clipsFilled[y][x] == true:
		//		fmt.Println("hightlighted is filled clip")
		color = colorFilledSelectedClip
	case c.clipsFilled[y][x] == false:
		//		fmt.Println("hightlighted is empty clip")
		color = colorEmptyClipSelected
	}
	c.RUnlock()
	c.Set(x, y, color)
}

func (c *clipSubmode) Enter() {
	c.Lock()
	c.isActiveSubmode = true
	c.Unlock()
	c.playtimeMode.paintControls()
	c.paintClips()
}

func (c *clipSubmode) Leave() {
	c.Lock()
	c.isActiveSubmode = false
	c.Unlock()
}

func (t *clipSubmode) Set(x, y uint8, color linnstrument.Color) {
	t.RLock()
	isActive := t.isActiveSubmode
	t.RUnlock()

	if isActive {
		t.tr.SetColor(x, y+1, color)
	}
}

func (c *clipSubmode) PadPressed(x, y, velocity uint8) {
	c.selectClip(x, y)
}

func (c *clipSubmode) selectClip(x, y uint8) {
	if x > 6 || y > 13 {
		return
	}
	//	fmt.Printf("unselecting clip %v/%v\n", x, y)
	c.playtime.SelectClip(y+1, x+1)
	c.unSelectClip()
	c.Lock()
	c.selectedClip[0] = int8(x)
	c.selectedClip[1] = int8(y)
	c.Unlock()

	c.highLightSelectedClip()
}

func (c *clipSubmode) unSelectClip() {
	x, y, has := c.getSelectedClip()
	if !has {
		return
	}

	//	fmt.Printf("unselecting clip %v/%v\n", x, y)

	var color linnstrument.Color

	c.Lock()
	c.selectedClip[0] = -1
	c.Unlock()
	c.RLock()
	switch {
	case c.recordingClips[y] == int8(x):
		//		fmt.Println("unselected clip was recording")
		color = colorClipRecording
	case c.activeTrackClips[y] == int8(x):
		//		fmt.Println("unselected clip was active")
		color = colorActiveTrackClip
	case c.clipsFilled[y][x] == true:
		//		fmt.Println("unselected clip was filled")
		color = colorFilledClip
	case c.clipsFilled[y][x] == false:
		//		fmt.Println("unselected clip was empty")
		color = OFF
	}
	c.RUnlock()
	c.Set(x, y, color)
}

func (c *clipSubmode) PadReleased(x, y uint8) {
}
