package linnreap

import "gitlab.com/golinnstrument/linnstrument"

const (
	RED     = linnstrument.ColorRed
	YELLOW  = linnstrument.ColorYellow
	GREEN   = linnstrument.ColorGreen
	CYAN    = linnstrument.ColorCyan
	BLUE    = linnstrument.ColorBlue
	MAGENTA = linnstrument.ColorMagenta
	OFF     = linnstrument.ColorOff
	WHITE   = linnstrument.ColorWhite
	ORANGE  = linnstrument.ColorOrange
	LIME    = linnstrument.ColorLime
	PINK    = linnstrument.ColorPink
)
