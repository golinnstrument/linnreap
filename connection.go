package linnreap

import (
	"fmt"
	"io"
	"os"
	"sync"
	"time"

	"gitlab.com/golinnstrument/linnstrument"
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/writer"
	"gitlab.com/goosc/osc"
)

// Triggerer is used by a Handler to send messages to the Reaper DAW
// and the LinnStrument Device. Also MIDI messages can be written to
// the MIDI out port.
type Triggerer interface {

	// The o.Writer to write to Reaper
	io.Writer

	// The LinnStrument device
	linnstrument.Device

	// Method to write to the MIDI out port
	WriteMIDI(midi.Message) error
}

type Handler interface {

	// Init is called once for the initialization of the handler
	Init(closer chan bool) (closed chan bool)

	// Matches returns if a given osc.Path should be handled
	Matches(path osc.Path) bool

	// SetTriggerer sets the given triggerer
	SetTriggerer(Triggerer)

	// OnMessage is called for an OSC message received from reaper
	OnMessage(path osc.Path, values ...interface{})

	// OnTouch is called, when a pad on the linnstrument was touched or released
	OnTouch(row, col, velocity uint8)

	// OnSlideTransition is called, when a slide transistion message was received from the LinnStrument
	OnSlideTransition(row, col uint8)

	// OnYData is called, when an y-axis data message was received from the LinnStrument
	OnYData(row, col, data uint8)

	// OnZData is called, when an y-axis data message was received from the LinnStrument
	OnZData(row, col, data uint8)

	// OnXDataMSB is called, when an x-axis MSB data message was received from the LinnStrument
	OnXDataMSB(row, col, msb uint8)

	// OnXDataLSB is called, when an x-axis LSB data message was received from the LinnStrument
	OnXDataLSB(row, col, lsb uint8)
}

// Connection represents a connection of a LinnStrument to the  Reaper DAW.
type Connection interface {
	Triggerer
	Matches(path osc.Path) bool
	Connect(dawConnection ReaperConnection, noteOut midi.Out, errHandler func(error)) error
	Handle(path osc.Path, values ...interface{})
}

// ReaperConnection represents an OSC connection to the  Reaper DAW.
type ReaperConnection interface {
	io.WriteCloser
	osc.Listener
	Connect() (err error)
}

func newConnection(name string, dev linnstrument.Device, h Handler) Connection {
	mw := &connection{}
	mw.name = name
	mw.Device = dev
	mw.actor = h
	return mw
}

type connection struct {
	dawConnection ReaperConnection
	linnstrument.Device
	//	connections  []Device
	name   string
	actor  Handler
	closed bool
	sync.RWMutex
	isOn          bool
	closer        chan bool
	blinkerClosed chan bool
	// the port to write the note messages to
	midiOut        midi.Out
	midiNoteWriter midi.Writer
}

func (m *connection) String() string {
	//return fmt.Sprintf("%s%d", m.name, m.Device.NumButtons())
	return m.name
}

func (m *connection) Set(x, y uint8, color linnstrument.Color) error {
	if m.closed {
		return nil
	}

	if x > m.Device.Rows() || y > m.Device.Cols() {
		return nil
	}
	err := m.Device.SetColor(x, y, color)
	if err != nil {
		fmt.Fprintf(os.Stderr, "can't write to linnstrument%v: %v", m.NumButtons(), err.Error())
	}
	return err
}

func (m *connection) Write(b []byte) (int, error) {
	if m.closed {
		return 0, nil
	}
	return m.dawConnection.Write(b)
}

func (m *connection) WriteMIDI(msg midi.Message) error {
	//m.Lock()
	err := m.midiNoteWriter.Write(msg)
	//m.Unlock()
	return err
}

var _ linnstrument.Listener = &connection{}

func (c *connection) OnSlideTransition(row, col uint8) {
	c.actor.OnSlideTransition(row, col)
}

func (c *connection) OnYData(row, col, data uint8) {
	c.actor.OnYData(row, col, data)
}

func (c *connection) OnZData(row, col, data uint8) {
	c.actor.OnZData(row, col, data)
}

func (c *connection) OnXDataLSB(row, col, lsb uint8) {
	c.actor.OnXDataLSB(row, col, lsb)
}

func (c *connection) OnXDataMSB(row, col, msb uint8) {
	c.actor.OnXDataMSB(row, col, msb)
}

func (d *connection) OnUserFirmwareMode(enabled bool, restoreColors func()) {
	if enabled {
		fmt.Println("on")
		//		time.Sleep(time.Millisecond * 600)
		d.Lock()
		d.isOn = true
		d.Unlock()
		//		d.All(linnstrument.Off)
		//time.Sleep(time.Millisecond * 400)
		//		if first {
		//			first = false
		//			fmt.Println("starting")
		//		} else {
		fmt.Println("refreshing")
		restoreColors()
		return
	}

	fmt.Println("off")
	d.Lock()
	d.isOn = false
	d.Unlock()
}

func (d *connection) Connect(dawConnection ReaperConnection, midiOut midi.Out, errHandler func(error)) error {
	d.dawConnection = dawConnection
	err := d.dawConnection.Connect()

	if err != nil {
		return err
	}

	if !midiOut.IsOpen() {
		err = midiOut.Open()

		if err != nil {
			return err
		}
	}

	d.midiOut = midiOut
	d.midiNoteWriter = writer.New(d.midiOut)

	d.actor.SetTriggerer(d)
	d.Device.StartListening(d)
	//	fmt.Println("printing greeter")
	//linnstrument.Greeter(d.Device)
	//	d.Device.All(linnstrument.Off)
	d.closer = make(chan bool)
	time.Sleep(time.Second)

	d.blinkerClosed = d.actor.Init(d.closer)
	return nil
}

func (m *connection) Close() error {
	if m.closed {
		return nil
	}
	m.Lock()
	m.closed = true
	m.Unlock()
	fmt.Println("closing midiout for notes")
	m.midiOut.Close()
	fmt.Println("closing blinker")
	m.closer <- true
	<-m.blinkerClosed
	fmt.Println("stop listening for daw events")
	m.dawConnection.StopListening()
	//	fmt.Println("done")
	fmt.Println("closing device")
	m.Device.Close()
	//	fmt.Println("done")
	fmt.Println("stop the daw connection")
	m.dawConnection.Close()
	//	fmt.Println("done")
	return nil
}

func (m *connection) Matches(path osc.Path) bool {
	return m.actor.Matches(path)
}

// Handle reaper events
func (m *connection) Handle(path osc.Path, values ...interface{}) {
	m.actor.OnMessage(path, values...)
}

// a pad was touched
func (m *connection) OnTouch(row, col, velocity uint8) {
	m.actor.OnTouch(row, col, velocity)
}
