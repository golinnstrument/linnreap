package linnreap

import (
	"fmt"

	"gitlab.com/golinnstrument/linnstrument"
)

var _ SubMode = &settingsSubmode{}

func newSettingsSubmode(tr Triggerer, pm *playSubmode) *settingsSubmode {
	m := &settingsSubmode{}
	m.tr = tr
	m.playSubmode = pm
	return m
}

type settingsSubmode struct {
	tr          Triggerer
	playSubmode *playSubmode
}

func (c *settingsSubmode) String() string {
	return "settings submode"
}

func (c *settingsSubmode) clearAll() {
	for x := uint8(0); x < c.tr.Rows(); x++ {
		for y := uint8(0); y < c.tr.Cols(); y++ {
			c.tr.SetColor(x, y, OFF)
		}
	}
}

func (*settingsSubmode) SplitPressed() linnstrument.Color {
	return OFF
}

func (*settingsSubmode) Switch1Pressed() linnstrument.Color {
	return OFF
}

func (*settingsSubmode) Switch1Released() linnstrument.Color {
	return OFF
}

func (*settingsSubmode) TransposePressed() linnstrument.Color {
	return OFF
}

func (*settingsSubmode) TransposeReleased() linnstrument.Color {
	return OFF
}

func (*settingsSubmode) SplitReleased() linnstrument.Color {
	return OFF
}

func (s *settingsSubmode) Enter() {
	s.clearAll()
	s.paintMIDIChannel()
	s.paintTimbreY()
	s.paintFullVelocity()
	s.paintLoudnessZ()
	s.paintPitchXActive()
	s.paintBendRange()
}

func (s *settingsSubmode) paintFullVelocity() {
	s.playSubmode.RLock()
	isFullvelocity := s.playSubmode.fullVelocity
	s.playSubmode.RUnlock()
	if isFullvelocity {
		s.Set(0, 0, GREEN)
	} else {
		s.Set(0, 0, OFF)
	}
}

func (s *settingsSubmode) paintTimbreY() {
	cc, isActive := s.playSubmode.getTimbreY()

	if isActive {
		s.Set(0, 8, GREEN)
	} else {
		s.Set(0, 8, OFF)
	}

	if cc == 74 {
		s.Set(1, 8, OFF)
		s.Set(2, 8, GREEN)
	} else {
		s.Set(1, 8, GREEN)
		s.Set(2, 8, OFF)
	}
}

func (s *settingsSubmode) paintLoudnessZ() {
	mode, isActive := s.playSubmode.getLoudnessZ()

	if isActive {
		s.Set(0, 9, GREEN)
	} else {
		s.Set(0, 9, OFF)
	}

	switch mode {
	case 0:
		s.Set(1, 9, GREEN)
		s.Set(2, 9, OFF)
		s.Set(3, 9, OFF)
	case 1:
		s.Set(1, 9, OFF)
		s.Set(2, 9, GREEN)
		s.Set(3, 9, OFF)
	case 2:
		s.Set(1, 9, OFF)
		s.Set(2, 9, GREEN)
		s.Set(3, 9, OFF)
	}
}

func (s *settingsSubmode) paintPitchXActive() {
	s.playSubmode.RLock()
	pitchXActive := s.playSubmode.pitchXActive
	s.playSubmode.RUnlock()

	if pitchXActive {
		s.Set(0, 7, GREEN)
	} else {
		s.Set(0, 7, OFF)
	}
}

func (s *settingsSubmode) paintMIDIChannel() {
	ch := s.getMIDIChannel()

	y := ch%4 + 2
	x := ch / 4

	for x := uint8(0); x < 4; x++ {
		for y := uint8(2); y < 6; y++ {
			s.Set(x, y, OFF)
		}
	}

	s.Set(x, y, GREEN)
}

func (s *settingsSubmode) paintBendRange() {
	s.playSubmode.RLock()
	bendrange := s.playSubmode.bendRange
	s.playSubmode.RUnlock()

	switch bendrange {
	case 0:
		s.Set(0, 6, GREEN)
		s.Set(1, 6, OFF)
		s.Set(2, 6, OFF)
		s.Set(3, 6, OFF)
	case 1:
		s.Set(0, 6, OFF)
		s.Set(1, 6, GREEN)
		s.Set(2, 6, OFF)
		s.Set(3, 6, OFF)
	case 2:
		s.Set(0, 6, OFF)
		s.Set(1, 6, OFF)
		s.Set(2, 6, GREEN)
		s.Set(3, 6, OFF)
	case 3:
		s.Set(0, 6, OFF)
		s.Set(1, 6, OFF)
		s.Set(2, 6, OFF)
		s.Set(3, 6, GREEN)

	}
}

func (*settingsSubmode) Leave() {

}

func (t *settingsSubmode) Set(x, y uint8, color linnstrument.Color) {
	t.tr.SetColor(x, y+1, color)
}

func (c *settingsSubmode) PadPressed(x, y, velocity uint8) {
	switch {
	case x == 0 && y == 0:
		c.playSubmode.Lock()
		isFullvelocity := c.playSubmode.fullVelocity
		c.playSubmode.fullVelocity = !isFullvelocity
		c.playSubmode.Unlock()
		if !isFullvelocity {
			c.Set(0, 0, GREEN)
		} else {
			c.Set(0, 0, OFF)
		}

	case x < 4 && y == 6:
		c.playSubmode.Lock()
		c.playSubmode.bendRange = x
		c.playSubmode.Unlock()
		c.paintBendRange()

	case x == 0 && y == 9:
		c.playSubmode.Lock()
		loudnessZActive := c.playSubmode.loudnessZActive
		c.playSubmode.loudnessZActive = !loudnessZActive
		c.playSubmode.Unlock()
		c.playSubmode.setLoudnessZData(!loudnessZActive)
		if !loudnessZActive {
			c.Set(0, 9, GREEN)
		} else {
			c.Set(0, 9, OFF)
		}

	case x == 1 && y == 9:
		c.playSubmode.Lock()
		c.playSubmode.loudnessZCC = 0
		c.playSubmode.Unlock()
		c.Set(1, 9, GREEN)
		c.Set(2, 9, OFF)
		c.Set(3, 9, OFF)

	case x == 2 && y == 9:
		c.playSubmode.Lock()
		c.playSubmode.loudnessZCC = 1
		c.playSubmode.Unlock()
		c.Set(1, 9, OFF)
		c.Set(2, 9, GREEN)
		c.Set(3, 9, OFF)

	case x == 3 && y == 9:
		c.playSubmode.Lock()
		c.playSubmode.loudnessZCC = 2
		c.playSubmode.Unlock()
		c.Set(1, 9, OFF)
		c.Set(2, 9, OFF)
		c.Set(3, 9, GREEN)

	case x == 0 && y == 8:
		c.playSubmode.Lock()
		timbreYActive := c.playSubmode.timbreYActive
		c.playSubmode.timbreYActive = !timbreYActive
		c.playSubmode.Unlock()
		c.playSubmode.setTrimbreYData(!timbreYActive)
		if !timbreYActive {
			c.Set(0, 8, GREEN)
		} else {
			c.Set(0, 8, OFF)
		}
	case x == 1 && y == 8:
		c.playSubmode.Lock()
		timbreYCC74 := c.playSubmode.timbreYCC74
		c.playSubmode.timbreYCC74 = !timbreYCC74
		c.playSubmode.Unlock()
		if timbreYCC74 {
			c.Set(1, 8, GREEN)
			c.Set(2, 8, OFF)
		}

	case x == 2 && y == 8:
		c.playSubmode.Lock()
		timbreYCC74 := c.playSubmode.timbreYCC74
		c.playSubmode.timbreYCC74 = !timbreYCC74
		c.playSubmode.Unlock()
		if !timbreYCC74 {
			c.Set(2, 8, GREEN)
			c.Set(1, 8, OFF)
		}

	case x == 0 && y == 7:
		c.playSubmode.Lock()
		pitchXActive := c.playSubmode.pitchXActive
		c.playSubmode.pitchXActive = !pitchXActive
		c.playSubmode.Unlock()
		c.playSubmode.setPitchXData(!pitchXActive)
		if !pitchXActive {
			c.Set(0, 7, GREEN)
		} else {
			c.Set(0, 7, OFF)
		}

	case y > 1 && y < 6 && x < 4:
		ch := (y - 2) + (x * 4)
		c.setMIDIChannel(ch)
		c.paintMIDIChannel()
	default:
		fmt.Printf("settings key %v/%v\n", x, y)
	}
}

func (c *settingsSubmode) getMIDIChannel() (ch uint8) {
	c.playSubmode.RLock()
	ch = c.playSubmode.midiChannel
	c.playSubmode.RUnlock()
	return
}

func (c *settingsSubmode) setMIDIChannel(ch uint8) {
	fmt.Printf("midi channel set to %v\n", ch)
	c.playSubmode.Lock()
	c.playSubmode.midiChannel = ch
	c.playSubmode.Unlock()
}

func (c *settingsSubmode) PadReleased(x, y uint8) {
}
