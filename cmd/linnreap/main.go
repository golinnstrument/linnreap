package main

import (
	"fmt"
	"os"
	"os/signal"
	"time"

	"gitlab.com/golinnstrument/linnreap"
	"gitlab.com/golinnstrument/linnstrument"
	"gitlab.com/gomidi/midi"

	driver "gitlab.com/gomidi/midicatdrv"
	"gitlab.com/goosc/reaper"
	"gitlab.com/metakeule/config"
)

var (
	cfg          = config.MustNew("linnreap", "0.1.1", "control reaper and playtime with linnstrument")
	outNotesMIDI = cfg.LastInt32("notes", "MIDI out port where the notes should be send to.",
		config.Required)
	inAddr = cfg.NewString("rin", "address where reaper sends to and linnstrument should receive from.",
		config.Default("127.0.0.1:9001"))
	outAddr = cfg.NewString("rout", "address where reaper receives from and linnstrument should sends to.",
		config.Default("127.0.0.1:8001"))
	outMIDI = cfg.NewInt32("lout", "number of the MIDI output port for the linnstrument", config.Shortflag('o'))
	inMIDI  = cfg.NewInt32("lin", "number of the MIDI input port for the linnstrument", config.Shortflag('i'))
	listCmd = cfg.MustCommand("ports", "list MIDI in and out ports").Relax("notes")
)

func run() error {
	err := cfg.Run()
	if err != nil {
		fmt.Fprintln(os.Stdout, cfg.Usage())
		return err
	}

	drv, err := driver.New()

	if err != nil {
		return err
	}

	defer drv.Close()

	if cfg.ActiveCommand() == listCmd {
		return printMIDIPorts(drv)
	}

	var linnstr linnstrument.Device

	if outMIDI.IsSet() && inMIDI.IsSet() {
		out, err := midi.OpenOut(drv, int(outMIDI.Get()), "")
		if err != nil {
			return err
		}

		in, err := midi.OpenIn(drv, int(inMIDI.Get()), "")
		if err != nil {
			return err
		}

		linnstr = linnstrument.New128(in, out)
	} else {

		linnstr, err = linnstrument.Find128(drv)

		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not find LinnStrument by name. Maybe not connected via USB?\nTry to pass the port numbers via --in and --out parameters.\n")
			return printMIDIPorts(drv)
		}
	}

	noteOut, err := midi.OpenOut(drv, int(outNotesMIDI.Get()), "")
	if err != nil {
		return err
	}

	m := linnreap.New128(linnstr)

	var sigchan = make(chan os.Signal, 10)

	err = m.Connect(reaper.New(m, reaper.ListenAddress(inAddr.Get()), reaper.WriteAddress(outAddr.Get())), noteOut, func(err error) {
		//		sigchan <- os.Interrupt
	})

	if err != nil {
		return err
	}

	fmt.Fprintf(os.Stdout, "listening on %v and writing to %v\n", inAddr.Get(), outAddr.Get())

	// listen for ctrl+c
	go signal.Notify(sigchan, os.Interrupt)

	// interrupt has happend
	<-sigchan

	fmt.Fprint(os.Stdout, "\ninterrupted, cleaning up...\n")
	//	m.All(linnstrument.Off)
	m.Close()
	time.Sleep(time.Second * 2)
	fmt.Fprintln(os.Stdout, "exit")
	//os.Exit(0)
	return nil
}

func main() {
	// this is to check, if the midicat binary is in your PATH. Uncomment, if you want to use another driver
	err := driver.CheckMIDICatBinary(os.Stdout)

	if err != nil {
		os.Exit(1)
	}

	if err = run(); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		os.Exit(1)
	}
	os.Exit(0)
}

func printMIDIPorts(drv midi.Driver) error {
	outs, err := drv.Outs()

	if err != nil {
		return err
	}

	fmt.Fprint(os.Stdout, "\n\n######### MIDI outputs #########\n\n")

	for _, out := range outs {
		fmt.Fprintf(os.Stdout, "[%v] %s\n", out.Number(), out.String())
	}

	ins, err := drv.Outs()

	if err != nil {
		return err
	}

	fmt.Fprint(os.Stdout, "\n\n######### MIDI inputs #########\n\n")

	for _, in := range ins {
		fmt.Fprintf(os.Stdout, "[%v] %s\n", in.Number(), in.String())
	}
	return nil
}
